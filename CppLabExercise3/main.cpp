// Lab Exercise 3 - Mad Lib
// Date: 03/10/2020
// Written By: Jordan Maxwell

#include <fstream>
#include <iostream>
#include <string>
#include <cstdint>

// constant defining the number of user inputs to collect.
// This variable should match to the number of inputs in the 
// user_prompts constant.
const int input_count = 12;

// constant containing all the user inputs
const std::string user_prompts[] = {
    "Enter an adjective (describing word): ",
    "Enter a sport: ",
	"Enter a city name: ",
    "Enter a person: ",
    "Enter an action verb (past tense): ",
    "Enter a vehicle: ",
    "Enter a place: ",
    "Enter a noun (thing, plural): ",
    "Enter an adjective (describing word): ",
    "Enter a food (plural): ",
    "Enter a liquid: ",
    "Enter an adjective (describing word): "
};

/* Handles users string input */
std::string get_string_input(std::string message) {

	// Prompt and retrieve input
	std::string input;
	std::cout << message;
	std::getline(std::cin, input);

	return input;
}

/* Handles users confirmation input */
bool get_confirm_input(std::string question) {
	// Prompt and retrieve input
	char input;
	std::cout << question << " [y/n]: ";
	std::cin >> input;

	// Clear cin of any remaining data
	std::cin.clear();
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	return input == 'y';
}

/* Constructs a madlib string from a list of user inputs */
std::string construct_madlib(std::string inputs[]) {
    std::string output = "";

	// Build madlib.
	output += "One day my " + inputs[0] + " friend and I decided to go to the " + inputs[1] + " game in " + inputs[2] + ".\n";
	output += "We really wanted to see " + inputs[3] + " play.\n";
	output += "So we " + inputs[4] + " in the " + inputs[5] + " and headed down to " + inputs[6] + " and bought some " + inputs[7] + ".\n";
	output += "We watched the game and it was " + inputs[8] + ".\n";
	output += "We ate some " + inputs[9] + " and drank some " + inputs[10] + ".\n";
	output += "We had a " + inputs[11] + " time, and can't wait to go again.\n";

    return output;
}

/* Saves the requested string to disk using the provided filename */
void save_to_file(std::string message, std::string filename) {
    std::ofstream out (filename);
    out << message;
    out.close();
}

/* Handles user input */
bool perform_input() {

    // Retrieve the users prompts
    std::string inputs[input_count];
    for (int i=0; i<input_count; i++) {
        inputs[i] = get_string_input(user_prompts[i]);
    }

	// Add some line breaks
	std::cout << std::endl;
	std::cout << std::endl;

    // Construct the madlib story string from the user inputs and
	// display the results to the user
    std::string madlib_story = construct_madlib(inputs);
	std::cout << madlib_story << std::endl << std::endl;

    // Ask the user if they would like to save their madlib. if the user
    // enters yes then ask them what they would like to call the file
    bool save = get_confirm_input("Would you like to save your madlib?");
    if (save) {
        std::string filename = get_string_input("What would you like to call the file? ");
        save_to_file(madlib_story, filename);
    }

    // Return true to inform the loop we would like to exit
    return true;
}

/* Handles the primary input loop */
void perform_input_loop() {
	bool should_exit = false;
	while (!should_exit) {
		should_exit = perform_input();
	}
}

/* Main entry point for the application */
int main() {
    perform_input_loop();
    return 0;
}